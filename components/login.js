Vue.component('login', {
  template: `<div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="lead-form">
          <h1 class="text-center">Please Login</h1>
          <hr/>
          <div class="row">
            <div class="alert alert-danger" v-if="loginFailed">
              <strong>Error!</strong> Login Failed.
            </div>
            <div class="">
              <input v-model="email" type="text" class="form-control" placeholder="email">
            </div>
            <div class="">
              <input v-model="password" type="text" class="form-control" placeholder="password">
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <button @click="login" class="btn btn-primary btn-block" id="submit-form">Submit</button>
            </div>
          </div>
        </div>
      </div>
    </div>`,
  data() {
    return{
      email: '',
      password: '',
      loginFailed: false
    }
  },
  methods: {
    login: function () {
      axios.post('http://localhost:8000/login/token/', {
        "email": this.email,
        "password": this.password
      }).then((response) => {
        this.$emit('token', response.data.token)
        this.token = response.data.token
        localStorage.setItem("token", response.data.token);
      }).catch((error) => {
        this.loginFailed = true
        console.log(error.statusText)
      });
    }
  },
})